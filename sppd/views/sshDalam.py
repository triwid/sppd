from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import SshDalamDaerah
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *


@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'SshDalamDaerah'
    page = request.GET.get('page', 1)
    sshDalam = SshDalamDaerah.objects.all().order_by('id').values()
    paginator = Paginator(sshDalam, 200)
    try:
        sshDalam = paginator.page(page)
    except PageNotAnInteger:
        sshDalam = paginator.page(1)
    except EmptyPage:
        sshDalam = paginator.page(paginator.num_pages)
    contex = {
        'title' : title,
        'sshDalam' : sshDalam
    }
    return render(request, 'master/master_ssh/ssh_dalam/index.html', contex)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = SshDalamDaerah.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except SshDalamDaerah.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def create(request):
    if request.method == 'POST':
        insert = SshDalamDaerah()
        tipe = request.POST.get('tipe')
        zona = request.POST.get('zona')
        satuan = request.POST.get('satuan')
        uraian = request.POST.get('uraian')
        harga = request.POST.get('harga')
        type_transportasi = request.POST.get('type_tras')
        keterangan = request.POST.get('ket')
        if insert is not None:
            insert.tipe = tipe
            insert.zona = zona
            insert.satuan = satuan
            insert.uraian = uraian
            insert.harga = harga
            insert.type_transportasi = type_transportasi
            insert.keterangan = keterangan
            insert.save()
            
            messages.success(request, 'Kata Mereka berhasil disimpan.')
            return redirect('sppd:sshdalam')

@login_required
@is_verified()
def edit(request, id):
    if request.method == 'POST':
        edit = SshDalamDaerah.objects.get(id = id)
        tipe = request.POST['tipeedit']
        zona = request.POST['zonaedit']
        satuan = request.POST['satuanedit']
        uraian = request.POST['uraianedit']
        harga = request.POST['hargaedit']
        type_transportasi = request.POST['type_trasedit']
        keterangan = request.POST['ketedit']
        if edit is not None:
                edit.tipe = tipe
                edit.zona = zona
                edit.satuan = satuan
                edit.uraian = uraian
                edit.harga = harga
                edit.type_transportasi = type_transportasi
                edit.keterangan = keterangan
                edit.save()

                messages.success(request, 'Kata Mereka berhasil disimpan.')
                return redirect('sppd:sshdalam')

