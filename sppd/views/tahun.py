from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import MasterTahun
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *


@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'MasterTahun'
    page = request.GET.get('page', 1)
    tahun = MasterTahun.objects.all().order_by('id').values()
    paginator = Paginator(tahun, 200)
    try:
        tahun = paginator.page(page)
    except PageNotAnInteger:
        tahun = paginator.page(1)
    except EmptyPage:
        tahun = paginator.page(paginator.num_pages)
    contex = {
        'title' : title,
        'tahun' : tahun
    }
    return render(request, 'master/master_tahun/index.html', contex)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = MasterTahun.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except MasterTahun.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def create(request):
    if request.method == 'POST':
        insert = MasterTahun()
        tahun = request.POST.get('tahun')
        if insert is not None:
            insert.tahun = tahun
            insert.save()
            
            messages.success(request, 'Kata Mereka berhasil disimpan.')
            return redirect('sppd:tahun')

@login_required
@is_verified()
def edit(request, id):
    if request.method == 'POST':
        edit = MasterTahun.objects.get(id = id)
        tahun = request.POST['thnEdit']
        if edit is not None:
                edit.tahun = tahun
                edit.save()

                messages.success(request, 'Kata Mereka berhasil disimpan.')
                return redirect('sppd:tahun')

@login_required
@is_verified()
def Off(request, id=""):
    MasterTahun.objects.filter(id=id).update(status = 0)
    return redirect('sppd:tahun')


@login_required
@is_verified()
def On(request, id=""):
    MasterTahun.objects.filter(id=id).update(status = 1)
    return redirect('sppd:tahun')
