from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime

from ..models import MasterOrganisasi
from ..models import MasterLokasi
from ..models import MasterKegiatan
from ..models import MasterPegawai
from ..models import MasterPengesah
from ..models import SshLuarDaerah
from ..models import Spt
from ..models import DetailSpt
from ..models import Sppd
from ..models import Spj
from ..models import DetailSpj
from ..models import DetailSppd
from django.db import IntegrityError
from django.db.models import Case, When, Q

from django.utils.formats import date_format
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *
from django.core.files.storage import FileSystemStorage
from django.db import transaction


import pprint


@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'Membuat SPPD'
    orgid = request.user.id_organisasi_id
    if request.user.hak_akses == 'Admin' :
        sppd = Sppd.objects.prefetch_related('spj_set').order_by('-id')
    else:
        sppd = Sppd.objects.prefetch_related('spj_set').filter( no_spt_id__kd_organisasi_id = orgid ).order_by('-id')
    pengesah = MasterPengesah.objects.all()
    contex = {
        'title' : title,
        'sppd' : sppd,
        'pengesah' : pengesah,
    }
    return render(request, 'SPJ/index.html', contex)

@login_required
@is_verified()
def LuarDaerah(request, id):
    title = 'Membuat SPJ'
    
    
    organisasi= MasterOrganisasi.objects.all().order_by('id_organisasi').values()
    lokasi = MasterLokasi.objects.all().order_by('id').values()
    kegiatan = MasterKegiatan.objects.all().order_by('id').values()
    pegawai = MasterPegawai.objects.filter(id=id)
    sshluar = SshLuarDaerah.objects.filter(jenis='Luar Provinsi')
    sshdalam = SshLuarDaerah.objects.filter(jenis='Dalam Provinsi')
    # print(sshdalam.query)
    no_spj = request.POST.get('nospj')
    spt = Spt.objects.prefetch_related('detailspt_set', 'sppd_set').filter(id=id).all()

    sppdDetail = Sppd.objects.prefetch_related('detailsppd_set').filter(no_spt=id)

    data_sshdalam = []
    for detail_spt in spt:
        for data in detail_spt.detailspt_set.all():
            array_ssh = []
            dict_sshdalam = {}
            # arr_1 = []
            for item in sppdDetail:
                for x in item.detailsppd_set.all():
                    if x.pegawai.id == data.pegawai.id:
                            dict_sshdalam_ = {}
                            dict_sshdalam_ = {
                                'id': data.pegawai.id,
                                'nip': data.pegawai.nip,
                                'nama': data.pegawai.nama,
                                'jabatan': data.pegawai.jabatan.jabatan,
                                'uraian_ssh': x.uraian_ssh,
                                'satuan': x.satuan,
                                'nilai': x.nilai,
                                'volume': x.volume,
                                'subtotal': x.subtotal,
                    
                            }
                            array_ssh.append(dict_sshdalam_)
                        
                    
                   
            dict_sshdalam['nama'] = data.pegawai.nama
            dict_sshdalam['id'] = data.pegawai.id
            dict_sshdalam['nip'] = data.pegawai.nip
            dict_sshdalam['detail'] = array_ssh
            data_sshdalam.append(dict_sshdalam)

            total = [y['subtotal'] for x in data_sshdalam for y in x['detail']]
            total = sum(total)
    
    # pprint.pprint(data_sshdalam)
    contex = {
        'title' : title,
        'organisasi' : organisasi,
        'lokasi' : lokasi,
        'kegiatan' : kegiatan,
        'pegawai' : pegawai,
        'sshluar' : sshluar,
        'sshdalam' : sshdalam,
        'detailspt' : spt,
        'data_sshdalam':data_sshdalam,
        'total': total,
        'no_spj' : no_spj,
    }
    return render(request, 'SPJ/form_spj.html', contex)



@login_required
@is_verified()
def create(request):
    if request.method == 'POST':
        try:
            with transaction.atomic():
                image_file = request.FILES.get('image')

                # Get data from the POST form
                nospj = request.POST.get('no_spj')
                tgl_sppd = request.POST.get('tanggalsppd')
                nosppd = request.POST.get('nosppd')

                # Save the relative path to the image
                spj = Spj(
                    no_bukti=nospj,
                    tanggal=tgl_sppd,
                    status=1,
                    bukti=image_file,
                    no_sppd_id=nosppd,
                )
                spj.save()

                # Get the ID of the newly created Spj
                id_spj = spj.id

                list_realisasi = request.POST.getlist('realisasi_input')
                list_sisa = request.POST.getlist('sisa_input')
                list_pegawai = request.POST.getlist('pegawai_id')
                list_pengajuan = request.POST.getlist('subtotal')
                print(list_pengajuan, list_realisasi, list_sisa, image_file, list_pegawai)
                for pengajuan , realisasi, sisa, pegawai in zip(list_pengajuan, list_realisasi, list_sisa, list_pegawai):
                    detailspj = DetailSpj(
                        pengajuan=pengajuan,
                        realisasi=realisasi,
                        sisa=sisa,
                        pegawai_id=pegawai,
                        spj_id=id_spj,
                    )
                    detailspj.save()

                return redirect('sppd:spj_perjadin')

        except Exception as e:
            print(e)  
            return HttpResponse(f"Terjadi kesalahan: {str(e)}", status=500)
    return HttpResponse(f"Terjadi kesalahan", status=500)

@login_required
@is_verified()
def print_spj(request, id_sppd):
    if request.method == 'POST':
        title = 'Print SPPD'
        spj = Spj.objects.filter(no_sppd_id = id_sppd).prefetch_related('detailspj_set')
        nama = request.POST.get('pengesah')
        pengesah = MasterPengesah.objects.filter(pegawai_id = nama).all()
        tanggal_input = request.POST.get('tanggal')
        tanggal = datetime.strptime(tanggal_input, '%Y-%m-%d').date()
        tanggal_format = date_format(tanggal, format='j F Y', use_l10n=True)

        data_sshdalam = []

        for data in spj:
            id_pegawai_totals = {}  # Dictionary untuk menyimpan total realisasi, sisa, dan pengajuan per ID pegawai
            
            for detail in data.detailspj_set.all():
                # Mendapatkan ID pegawai dari detail
                id_pegawai = detail.pegawai.id
                
                # Memeriksa apakah ID pegawai sudah ada di dictionary
                if id_pegawai in id_pegawai_totals:
                    # Jika sudah ada, tambahkan nilai dari detail ke total yang sudah ada
                    id_pegawai_totals[id_pegawai]['realisasi'] += detail.realisasi
                    id_pegawai_totals[id_pegawai]['sisa'] += detail.sisa
                    id_pegawai_totals[id_pegawai]['pengajuan'] += detail.pengajuan
                else:
                    # Jika belum ada, buat entri baru dengan nilai dari detail
                    id_pegawai_totals[id_pegawai] = {
                        'realisasi': detail.realisasi,
                        'sisa': detail.sisa,
                        'pengajuan': detail.pengajuan,
                        'nama': detail.pegawai.nama,  # Tambahkan nama di sini
                        'no_sppd': data.no_sppd.no_sppd,  # Tambahkan no_sppd di sini
                        'tanggal': data.tanggal,  # Tambahkan tanggal di sini
                    }
                
            
            # Setelah loop detail, tambahkan data total per ID pegawai ke dalam data_sshdalam
            for id_pegawai, totals in id_pegawai_totals.items():
                dict_sshdalam = {
                    'id': id_pegawai,
                    'nama': totals['nama'],
                    'no_sppd': totals['no_sppd'],
                    'tanggal': totals['tanggal'],
                    'total_realisasi': totals['realisasi'],
                    'total_sisa': totals['sisa'],
                    'total_pengajuan': totals['pengajuan'],
                }
                data_sshdalam.append(dict_sshdalam)

            total_realisasi = 0
            total_sisa = 0
            total_pengajuan = 0

            for item in data_sshdalam:
                total_realisasi += item['total_realisasi']
                total_sisa += item['total_sisa']
                total_pengajuan += item['total_pengajuan']

        print(data_sshdalam)

        context = {
            'title': title,
            'data_sshdalam': data_sshdalam,
            'nama': nama,
            'pengesah': pengesah,
            'tanggal': tanggal_format,
            'total_realisasi': total_realisasi,
            'total_sisa': total_sisa,
            'total_pengajuan': total_pengajuan,
        }
        return render(request, 'laporan/laporan/SPPD_rampung.html', context) 



@login_required
@is_verified()
def preview_spj(request, id_sppd):
    title = 'Print SPPD'

    # Prefetch related data from both detailspj_set and detailsppd_set
    spj = Spj.objects.filter(no_sppd_id=id_sppd).prefetch_related('detailspj_set__pegawai', 'no_sppd__detailsppd_set')

    data_sshdalam = []
    processed_names = set()

    subtotal_nilai = 0
    subtotal_realisasi = 0
    subtotal_sisa = 0

    # Tambahkan variabel detailsppd
    detailsppd = Spj.objects.filter(no_sppd_id=id_sppd).prefetch_related('detailspj_set__pegawai', 'no_sppd__detailsppd_set')

    for data in spj:
        for detail_sppd in data.no_sppd.detailsppd_set.all():
            no_nip = detail_sppd.pegawai.nip
            id_pegawai = detail_sppd.pegawai.id
            nama = detail_sppd.pegawai.nama
            jabatan = detail_sppd.pegawai.jabatan
            satuan = detail_sppd.satuan  # Include satuan information

            # Create a unique key based on name and ID
            key = f"{nama}_{id_pegawai}"

            # Check if the name has been processed
            if key not in processed_names:
                processed_names.add(key)

                id_pegawai_details = []

                # Fetch all relevant detail_spj instances for the current detail_sppd
                detail_spjs = data.detailspj_set.filter(pegawai=detail_sppd.pegawai)

                # Ambil satu data yang tidak sama dari detail_sppds
                # Menggunakan iterator untuk mendapatkan satu data setiap kali iterasi
                detail_sppd_entries_iter = iter(data.no_sppd.detailsppd_set.filter(pegawai=detail_sppd.pegawai))
                
                
                for detail_spj in detail_spjs:
                    # Ambil satu data dari detail_sppd_entries_iter yang tidak sama
                    detail_sppd_entry = next(detail_sppd_entries_iter)

                    # Gunakan ID sebagai identifikasi tambahan
                    unique_id = detail_sppd_entry.id
                    uraian_ssh_value = detail_sppd_entry.uraian_ssh
                    id_pegawai_details.append({
                        'pengajuan': detail_spj.pengajuan,
                        'sisa': detail_spj.sisa,
                        'realisasi': detail_spj.realisasi,
                        'uraian_ssh': uraian_ssh_value,
                        'jabatan': detail_sppd_entry.pegawai.jabatan,
                        'satuan': detail_sppd_entry.satuan,
                        'volume': detail_sppd_entry.volume,
                        'nilai': detail_sppd_entry.nilai,
                    })

                    subtotal_realisasi += detail_spj.realisasi
                    subtotal_sisa += detail_spj.sisa
                    subtotal_nilai += detail_spj.pengajuan

                dict_sshdalam = {
                    'no_nip': no_nip,
                    'id_pegawai': id_pegawai,
                    'nama': nama,
                    'jabatan': jabatan,
                    'details': id_pegawai_details,
                    
                }

                data_sshdalam.append(dict_sshdalam)

    print(data_sshdalam)

    # Ambil satu data dari detailsppd yang tidak sama
    detailsppd_local = detailsppd

    context = {
        'title': title,
        'data_sshdalam': data_sshdalam,
        'detailsppd': detailsppd_local,
        'subtotal_realisasi': subtotal_realisasi,
        'subtotal_sisa': subtotal_sisa,
        'subtotal_nilai': subtotal_nilai,
    }

    return render(request, 'SPJ/preview.html', context)


@login_required
@is_verified()
def download_file(request, file_id):
    spj = Spj.objects.get(id=file_id)
    response = HttpResponse(spj.bukti, content_type='application/force-download')
    response['Content-Disposition'] = f'attachment; filename="{spj.bukti}"'
    return response
































