from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *
from ..models import MasterOrganisasi
from ..models import MasterLokasi
from ..models import MasterKegiatan
from ..models import MasterPegawai
from ..models import MasterPengesah


# # Create your views here.
@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    orgid = request.user.id_organisasi_id
    if request.user.hak_akses == 'Admin' :
        spt = Spt.objects.prefetch_related('detailspt_set').all().order_by('-id')
    else:
        spt = Spt.objects.prefetch_related('detailspt_set').filter( kd_organisasi_id = orgid ).order_by('-id')
    title = 'Verifikasi SPT'
    contex = {
        'title' : title,
        'spt' : spt,
    }
    return render(request,'verifikasi_spt/index.html', contex)
    
@login_required
@is_verified()
def Approve(request, id):
    message = ''
    try:
        doc = Spt.objects.get(id=id)
        doc.status = 1
        doc.save()
        message = 'success'
    except Kata.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

    
@login_required
@is_verified()
def NotApprove(request, id):
    message = ''
    try:
        doc = Spt.objects.get(id=id)
        doc.status = 0
        doc.save()
        message = 'success'
    except Kata.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
