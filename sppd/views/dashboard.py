from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *
from django.db.models import Sum
from ..models import Sppd
from ..models import Spt
from ..models import MasterPegawai
import pprint

# Create your views here.
@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'Dashboard'
    sppd = Sppd.objects.aggregate(total_perjalanan=Sum('total_perjalanan')) 
    jumlah_spt = Spt.objects.count()
    jumlah_pegawai = MasterPegawai.objects.count()
    jumlah_sppd = Sppd.objects.count()
    total_biaya = sppd['total_perjalanan']

    # Perbaiki total_biaya dengan cara ini
    total_biaya = total_biaya if total_biaya else 0  # Untuk menghindari None jika aggregate tidak mengembalikan hasil

    # Hapus total_biaya sum(total_biaya) yang tidak perlu

    context = {
        'title': title,
        'total_biaya': total_biaya,
        'jumlah_spt': jumlah_spt,
        'jumlah_pegawai': jumlah_pegawai,
        'jumlah_sppd': jumlah_sppd,
    }
    return render(request, 'dashboard/index.html', context)