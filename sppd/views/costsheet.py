from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *
from ..models import Spt, MasterPengesah, Sppd, DetailSppd
from django.db.models import Sum
from collections import defaultdict
from datetime import datetime
from django.utils.formats import date_format

# # Create your views here.
@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'costsheet'
    spt = Spt.objects.prefetch_related('sppd_set').filter(status=1).order_by('id')
    pengesah = MasterPengesah.objects.all().order_by('id')
    contex = {
        'title' : title,
        'spt' : spt,
        'pengesah' : pengesah,
    
    }
    
    return render(request, 'rekap_laporan/detail_sppd/costsheet.html', contex)


def print_costsheet(request, id):
    title = 'costsheet'

    nama = request.POST.get('pengesah')
    pengesah = MasterPengesah.objects.filter(pegawai_id = nama).all()
    tanggal_input = request.POST.get('tanggal')
    tanggal = datetime.strptime(tanggal_input, '%Y-%m-%d').date()
    tanggal_format = date_format(tanggal, format='j F Y', use_l10n=True)
    
    spt = Spt.objects.prefetch_related('sppd_set', 'detailspt_set').filter(status=1, id=id).order_by('-id')
    spt_data = Spt.objects.prefetch_related('sppd_set', 'sppd_set__detailsppd_set', 'detailspt_set').filter(status=1, id=id).all()
    spt_detail = get_object_or_404(Spt, status=1, id=id)

    total_perjalanan = sum(sppd.total_perjalanan for sppd in spt_detail.sppd_set.all())
    saldo_sekarang = int(spt_detail.kegiatan.pagu) - total_perjalanan

    detail_sppd = Sppd.objects.prefetch_related('detailsppd_set').filter(status=1 , no_spt_id=id).all()


    data_sppd = []
    detail = []
    hotel_all = 0
    harian_all = 0
    tiket_all = 0
    represen_all = 0
    transport_all = 0
    total_all = 0


    for data in spt_data:
        for x in data.detailspt_set.all():
            id_pegawai = x.pegawai.id
            nama = x.pegawai.nama
            jabatan = x.pegawai.jabatan.jabatan
            awal = data.kota_awal
            tujuan = data.kota_tujuan
            berangkat = data.tgl_berangkat
            kembali = data.tgl_kembali
            durasi = data.durasi

            for sppd_obj in data.sppd_set.all():
                dict_sppd = {}
                hotel = 0
                harian = 0
                represen = 0
                tiket = 0
                transport = 0
                for detail_sppd in sppd_obj.detailsppd_set.filter(pegawai__id=id_pegawai):
                    
                    if detail_sppd.uraian_ssh == 'Uang Harian/Lumpsum':
                        harian += detail_sppd.subtotal

                    if detail_sppd.uraian_ssh == 'Hotel/Penginapan':
                        hotel += detail_sppd.subtotal

                    if detail_sppd.uraian_ssh == 'Uang Representatif':
                        represen += detail_sppd.subtotal

                    if detail_sppd.uraian_ssh == 'Tiket':
                        tiket += detail_sppd.subtotal

                    if detail_sppd.uraian_ssh == 'Transport':
                        transport += detail_sppd.subtotal

            hotel_all += hotel
            harian_all += harian
            represen_all += represen
            tiket_all += tiket
            transport_all += transport


            total = harian + hotel + represen + tiket + transport

            total_all += total

            dict_sppd['id'] = id_pegawai
            dict_sppd['nama'] = nama
            dict_sppd['jabatan'] = jabatan
            dict_sppd['awal'] = awal
            dict_sppd['tujuan'] = tujuan
            dict_sppd['berangkat'] = berangkat
            dict_sppd['kembali'] = kembali
            dict_sppd['durasi'] = durasi
            dict_sppd['hotel'] = hotel
            dict_sppd['harian'] = harian
            dict_sppd['tiket'] = tiket
            dict_sppd['represen'] = represen
            dict_sppd['transport'] = transport
            dict_sppd['total'] = total
            data_sppd.append(dict_sppd)
    

    print(data_sppd)
    
    context = {
        'title': title,
        'spt': spt,
        'data_sppd': data_sppd,
        'harian_all': harian_all,
        'hotel_all': hotel_all,
        'tiket_all': tiket_all,
        'represen_all': represen_all,
        'transport_all': transport_all,
        'total_all': total_all,
        'saldo_sekarang': saldo_sekarang,
        'durasi': durasi,
    }

    return render(request, 'laporan/laporan/costsheet.html', context)




