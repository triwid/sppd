from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from django.http import JsonResponse
from datetime import datetime
from django.utils.formats import date_format
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *
from ..models import MasterOrganisasi
from ..models import MasterLokasi
from ..models import MasterKegiatan
from ..models import MasterPegawai
from ..models import MasterPengesah
from ..models import Spt
from ..models import DetailSpt

from pprint import pprint

# # Create your views here.
@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'Daftar SPT'
    orgid = request.user.id_organisasi_id
    pengesah = MasterPengesah.objects.all()
    if request.user.hak_akses == 'Admin' :
        spt = Spt.objects.prefetch_related('detailspt_set').all().order_by('-id')
    else:
        spt = Spt.objects.prefetch_related('detailspt_set').filter( kd_organisasi_id = orgid ).order_by('-id')
    
    
    contex = {
        'title' : title,
        'spt' : spt,
        'pengesah' : pengesah,
    }
    return render(request,'SPT/index.html',contex)

@login_required
@is_verified()
def create_spt(request):
    title = 'Create SPT'
    userid = request.user.id_organisasi_id
    if userid is not None:
        organisasi = MasterOrganisasi.objects.filter(id_organisasi=userid).all()
    else:
        organisasi = MasterOrganisasi.objects.all().order_by('id_organisasi').values()
    lokasi = MasterLokasi.objects.all().order_by('id').values()
    kegiatan = MasterKegiatan.objects.all().order_by('id').values()
    pegawai = MasterPegawai.objects.all().order_by('id')
    id_spt = Spt.objects.all().order_by('-id')[:1]
    
    contex = {
        'title' : title,
        'organisasi' : organisasi,
        'lokasi' : lokasi,
        'kegiatan' : kegiatan,
        'pegawai' : pegawai,
        'idnewspt' : id_spt,
    }

    return render(request,'SPT/create_spt.html',contex)

@login_required
@is_verified()
def create(request):
    if request.method == 'POST':
        try:
            # Ambil data dari form POST
            nospt = request.POST.get('nospt')
            organisasi = request.POST.get('organisasi')
            berangkat = request.POST.get('berangkat')
            kembali = request.POST.get('kembali')
            durasi = request.POST.get('durasi')
            status = request.POST.get('status')
            awal = request.POST.get('awal')
            tujuan = request.POST.get('tujuan')
            pembebanan = request.POST.get('pembebanan')
            kegiatan = request.POST.get('kegiatan')
            maksud = request.POST.get('maksud')
            created_by = request.user.id_user

            # Buat objek Spt dan simpan
            spt = Spt(
                no_spt=nospt,
                tgl_berangkat=berangkat,
                tgl_kembali=kembali,
                durasi=durasi,
                pembebanan_anggaran=pembebanan,
                status_perjalanan=status,
                kota_awal=awal,
                kota_tujuan=tujuan,  # Seharusnya ini adalah tujuan
                uraian=maksud,
                kd_organisasi_id=organisasi,
                kegiatan_id=kegiatan,
                created_by_id=created_by
            )
            spt.save()

            # Ambil ID Spt yang baru saja dibuat
            id_spt = spt.id

            # Buat objek DetailSpt untuk setiap pegawai
            data = request.POST.getlist('id_pegawai[]')
            for item in data:
                detail_spt = DetailSpt(
                    pegawai_id=item,
                    spt_id=id_spt  # Set ID Spt yang sesuai
                )
                detail_spt.save()

            return redirect('sppd:list_spt')
        
        except Exception as e:
            print(e)  # Tampilkan pesan kesalahan untuk debug

    return redirect('sppd:list_spt')  # Gantilah dengan template yang sesuai

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = get_object_or_404(Spt, id=id)
        dspt_set = DetailSpt.objects.filter(spt_id=id)
        
        for dspt in dspt_set:
            dspt.delete()
        
        doc.delete()
        message = 'success'
    except Spt.DoesNotExist:
        message = 'error'

    context = {
        'message': message,
    }

    return HttpResponse(context)

@login_required
@is_verified()
def preview(request, id_spt):
    spt = Spt.objects.filter(id = id_spt)
    context = {
            'title': 'Edit Surat Perintah Tugas',
            'spt': spt,
        }
    return render(request, 'SPT/preview_spt.html', context)

@login_required
@is_verified()
def print_spt(request, id_spt):
    if request.method == 'POST':
        title = 'Print SPT'
        nama = request.POST.get('pengesah')
        dt_pengesah = MasterPegawai.objects.filter(nama = nama)
        spt = Spt.objects.filter( id=id_spt )
        tanggal_input = request.POST.get('tanggal')
        tanggal = datetime.strptime(tanggal_input, '%Y-%m-%d').date()
        tanggal_format = date_format(tanggal, format='j F Y', use_l10n=True)


        print(nama, dt_pengesah)

        context = {
            'title': title,
            'dt_pengesah': dt_pengesah,
            'nama': nama,
            'tanggal': tanggal_format,
            'spt': spt,
        }
        return render(request, 'laporan/laporan/SPTM.html', context) 

@login_required
@is_verified()
def edit(request, id_spt):
    if request.method == 'GET':
        spt = Spt.objects.filter(id = id_spt).prefetch_related('detailspt_set')
        organisasi = MasterOrganisasi.objects.all()
        kegiatan = MasterKegiatan.objects.all()
        pegawai = MasterPegawai.objects.all()

        context = {
            'title': 'Edit Surat Perintah Tugas',
            'spt': spt,
            'organisasi': organisasi,
            'kegiatan': kegiatan,
            'pegawai': pegawai,
        }
        return render(request, 'SPT/form_edit.html', context)

    if request.method == 'POST':
        try:
            # Ambil data dari form POST
            nospt = request.POST.get('nospt')
            organisasi = request.POST.get('organisasi')
            berangkat = request.POST.get('berangkat')
            kembali = request.POST.get('kembali')
            durasi = request.POST.get('durasi')
            status = request.POST.get('status')
            awal = request.POST.get('awal')
            tujuan = request.POST.get('tujuan')  # Anda mungkin perlu mengganti ini
            pembebanan = request.POST.get('pembebanan')
            kegiatan = request.POST.get('kegiatan')
            maksud = request.POST.get('maksud')
            created_by = request.user.id_user

            # Cek apakah SPT yang akan diedit ada dalam database
            spt = Spt.objects.get(id=id_spt)

            # Perbarui data SPT yang ada
            spt.no_spt = nospt
            spt.tgl_berangkat = berangkat
            spt.tgl_kembali = kembali
            spt.durasi = durasi
            spt.pembebanan_anggaran = pembebanan
            spt.status_perjalanan = status
            spt.kota_awal = awal
            spt.kota_tujuan = tujuan
            spt.uraian = maksud
            spt.kd_organisasi_id = organisasi
            spt.kegiatan_id = kegiatan
            spt.created_by_id = created_by
            spt.save()

            # Hapus semua DetailSpt yang terkait dengan SPT yang akan diedit
            DetailSpt.objects.filter(spt_id=id_spt).delete()

            # Simpan DetailSpt yang baru
            data = request.POST.getlist('id_pegawai[]')
            for item in data:
                detail_spt = DetailSpt(
                    pegawai_id=item,
                    spt_id=id_spt
                )
                detail_spt.save()

            return redirect('sppd:list_spt')

        except Exception as e:
            print(e)  # Tampilkan pesan kesalahan untuk debug

    return redirect('sppd:list_spt')  # Gantilah dengan template yang sesuai


    
    # if request.method == 'POST':
    #     news = Kata.objects.get(id = id)
    #     judul = request.POST.get('judul')
    #     keterangan = request.POST.get('keterangan')

    #     if news is not None:
    #         news.judul = judul
    #         news.keterangan = keterangan
    #         news.save()

    #         messages.success(request, 'Kata Mereka berhasil disimpan')
    #         return redirect('profile:admin_KM')

    #     messages.error(request, 'Kata Mereka gagal disimpan.')
    #     return render(request, 'profile/admin/kata/create.html', {'form': form,})