
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, Group, Permission
from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from ckeditor.fields import RichTextField

class MasterOrganisasi(models.Model):
    id_organisasi = models.AutoField(primary_key=True)
    kode_organisasi = models.CharField(max_length=225, default='none')
    urai = models.CharField(max_length=225)

class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError(_("The Email must be set"))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)
        extra_fields.setdefault('is_verified', True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError(_("Superuser must have is_staff=True."))
        if extra_fields.get("is_superuser") is not True:
            raise ValueError(_("Superuser must have is_superuser=True."))
        if extra_fields.get('is_verified') is not True:
            raise ValueError(_("Superuser must have is_verified=True."))
        return self.create_user(email, password, **extra_fields)

class Account(AbstractBaseUser, PermissionsMixin):
    id_user = models.AutoField(primary_key=True)
    id_organisasi = models.ForeignKey(MasterOrganisasi, on_delete=models.CASCADE, null=True, blank=True)
    nama = models.CharField(max_length=40)
    email = models.EmailField(_("email address"), unique=True)
    hak_akses = models.CharField(max_length=30, default="-")
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_verified = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email

    groups = models.ManyToManyField(Group, related_name="user_set", blank=True)
    user_permissions = models.ManyToManyField(Permission, related_name="user_set", blank=True)

class MasterTahun(models.Model):
    tahun = models.CharField(max_length=4)
    status = models.BooleanField(default=False)

class MasterLokasi(models.Model):
    kd_lokasi = models.CharField(max_length=225)
    urai = models.TextField(max_length=225)

class MasterKegiatan(models.Model):
    kd_bidang = models.TextField(max_length=225)
    urai_bidang = RichTextField()
    kd_program = models.TextField(max_length=225)
    urai_program = RichTextField()
    kd_kegiatan = models.TextField(max_length=225)
    urai_kegiatan = RichTextField()
    kd_subkegiatan = models.TextField(max_length=225)
    urai_subkegiatan = RichTextField()
    pagu = models.TextField(max_length=225)
    pagu_p = models.TextField(max_length=225)
    sumber_dana = models.TextField(max_length=225)

class MasterJabatan(models.Model):
    jabatan = models.CharField(max_length=225)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.jabatan

class MasterPegawai(models.Model):
    nip = models.CharField(max_length=225,unique=True)
    nama = models.CharField(max_length=225)
    golongan = models.CharField(max_length=225)
    no_hp = models.CharField(max_length=225)
    eselon = models.CharField(max_length=225)
    alamat = models.CharField(max_length=225)
    pangkat = models.TextField(max_length=225)
    email = models.EmailField(max_length=225)
    jabatan = models.ForeignKey(MasterJabatan, on_delete=models.RESTRICT)  


class MasterPengesah(models.Model):
    no_rek = models.TextField(max_length=225)
    nama_bank = models.TextField(max_length=225)
    status = models.BooleanField(default=False)
    jabatan = models.ForeignKey(MasterJabatan, on_delete=models.RESTRICT)
    pegawai = models.ForeignKey(MasterPegawai, on_delete=models.RESTRICT)

class LogVisitor(models.Model):
	ip = models.CharField(max_length=20, default='-')
	device_type = models.CharField(max_length=20, default='-')
	browser_type = models.CharField(max_length=30, default='-')
	browser_version = models.CharField(max_length=30, default='-')
	os_type = models.CharField(max_length=30, default='-')
	os_version = models.CharField(max_length=30, default='-')
	waktu = models.DateTimeField(auto_now_add=True)

class LogAdmin(models.Model):
	ip = models.CharField(max_length=20, default='-')
	device_type = models.CharField(max_length=20, default='-')
	browser_type = models.CharField(max_length=30, default='-')
	browser_version = models.CharField(max_length=30, default='-')
	os_type = models.CharField(max_length=30, default='-')
	os_version = models.CharField(max_length=30, default='-')
	aktivitas = models.CharField(max_length=30, default='-')
	waktu = models.DateTimeField(auto_now_add=True)


class SshLuarDaerah(models.Model):
	jenis = models.CharField(max_length=225)
	tipe = models.CharField(max_length=225)
	satuan = models.CharField(max_length=225)
	uraian = models.CharField(max_length=225)
	harga = models.IntegerField(null=True)
	type_transportasi = models.CharField(max_length=225)
	keterangan = models.TextField(max_length=225)

class SshDalamDaerah(models.Model):
	tipe = models.CharField(max_length=225)
	zona = models.CharField(max_length=225) 
	satuan = models.CharField(max_length=225)
	uraian = models.CharField(max_length=225)
	harga = models.IntegerField(null=True)
	type_transportasi = models.CharField(max_length=225)
	keterangan = models.TextField(max_length=225)


class Spt(models.Model):
    no_spt = models.CharField(max_length=255)
    kd_organisasi = models.ForeignKey(MasterOrganisasi, on_delete=models.RESTRICT)
    tgl_berangkat = models.DateTimeField(null=True, blank=True)
    tgl_kembali = models.DateTimeField(null=True, blank=True)
    durasi = models.IntegerField(null=True)
    pembebanan_anggaran = models.CharField(max_length=225)
    kegiatan = models.ForeignKey(MasterKegiatan, on_delete=models.RESTRICT)
    status_perjalanan = models.CharField(max_length=255)
    kota_awal = models.CharField(max_length=255)
    kota_tujuan = models.CharField(max_length=255)
    uraian = RichTextField()
    status = models.BooleanField(null=True)
    created_by = models.ForeignKey(Account, on_delete=models.RESTRICT)

class DetailSpt(models.Model):
    spt = models.ForeignKey(Spt, on_delete=models.RESTRICT)
    pegawai = models.ForeignKey(MasterPegawai, on_delete=models.RESTRICT)

class Sppd(models.Model):
    no_sppd = models.CharField(max_length=225)
    no_spt = models.ForeignKey(Spt, on_delete=models.RESTRICT)
    tgl_sppd = models.DateTimeField(null=True, blank=True) 
    total_perjalanan = models.IntegerField(default=0)
    status = models.BooleanField(null=True)

    class Meta:
        unique_together = ('no_sppd', 'no_spt')

class DetailSppd(models.Model):
     no_sppd = models.ForeignKey(Sppd, on_delete=models.RESTRICT)
     pegawai = models.ForeignKey(MasterPegawai, on_delete=models.RESTRICT)
     uraian_ssh = RichTextField()
     satuan = models.CharField(max_length=225)
     volume = models.IntegerField(null=True)
     nilai = models.IntegerField(null=True)
     subtotal = models.IntegerField(null=True)

     class Meta:
          unique_together = ('no_sppd', 'pegawai', 'uraian_ssh')

class Spj(models.Model):
    no_bukti = models.CharField(max_length=225)
    bukti = models.ImageField(max_length=225, upload_to='bukti/', null=True)
    no_sppd = models.ForeignKey(Sppd, on_delete=models.RESTRICT)
    tanggal = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(null=True)

class DetailSpj(models.Model):
    spj = models.ForeignKey(Spj, on_delete=models.RESTRICT)
    pegawai = models.ForeignKey(MasterPegawai, on_delete=models.RESTRICT, null=True)
    pengajuan = models.IntegerField(default=0)
    realisasi = models.IntegerField(default=0)
    sisa = models.IntegerField(default=0)



     
     
     




	


        
