from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import Account
from ..models import MasterOrganisasi
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *


@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'Manage User'
    akun = Account.objects.all().order_by('id_user')
    organisasi = MasterOrganisasi.objects.all().order_by('id_organisasi')
    contex = {
        'title' : title,
        'akun' : akun,
        'organisasi' : organisasi,
    }
    return render(request, 'manage_user/index.html', contex)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = MasterTahun.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except MasterTahun.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def addUser(request):
    if request.method == "POST":
        nama = request.POST.get('nama')
        email = request.POST.get('email')
        hak_akses = request.POST.get('hak_akses')
        organisasi = request.POST.get('organisasi')
        password = request.POST.get('password')

        print(email)

        if Account.objects.filter(email=email).exists():
            return HttpResponse("Email sudah terdaftar.")
        

        insert = Account()
        insert.email = email
        insert.nama = nama
        insert.hak_akses = hak_akses
        insert.id_organisasi_id = organisasi
        insert.set_password(password)
        insert.save()

        return redirect('sppd:manage_user')


@login_required
@is_verified()
def create(request):
    if request.method == 'POST':
        insert = MasterTahun()
        tahun = request.POST.get('tahun')
        if insert is not None:
            insert.tahun = tahun
            insert.save()
            
            messages.success(request, 'Kata Mereka berhasil disimpan.')
            return redirect('sppd:tahun')

@login_required
@is_verified()
def edit(request, id):
    if request.method == 'POST':
        edit = MasterTahun.objects.get(id = id)
        tahun = request.POST['thnEdit']
        if edit is not None:
                edit.tahun = tahun
                edit.save()

                messages.success(request, 'Kata Mereka berhasil disimpan.')
                return redirect('sppd:tahun')

@login_required
@is_verified()
def Off(request, id=""):
    MasterTahun.objects.filter(id=id).update(status = 0)
    return redirect('sppd:tahun')

@login_required
@is_verified()
def On(request, id=""):
    MasterTahun.objects.filter(id=id).update(status = 1)
    return redirect('sppd:tahun')
# def addTahun(request):
#     if request.method == "POST":
#         tahun = request.POST.get('tahun')

#         insert = MasterTahun()
#         insert.tahun = tahun

#         insert.save()

#         return redirect('SPPD:tahun')

# def editTahun(request, idedit=""):
#     if request.method == "POST":
#         tahun = request.POST['thnEdit']

#         MasterTahun.objects.filter(id=idedit).update(tahun = tahun)

#         return redirect('SPPD:tahun')

# def hapusTahun(request, idedit):
#     hapus = MasterTahun.objects.get(id=idedit)

#     hapus.delete()
#     return redirect('SPPD:tahun')