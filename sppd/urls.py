from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'sppd'
urlpatterns = [
    path('', login.loginUser, name='login'),
    path('logout/', login.logoutUser, name='logout'),
    path('adduser/', login.addUser, name='adduser'),
    path('dash/', dashboard.index, name='dash'),
    path('master_tahun/', tahun.index, name='tahun'),
    path('master_tahun/tambah/', tahun.create, name='tambah'),
    path('master_tahun/hapus/<int:id>', tahun.permanentDelete, name='hapus'),
    path('master_tahun/edit/<int:id>', tahun.edit, name='edit'),
    path('master_tahun/off/<int:id>', tahun.Off, name='Off'),
    path('master_tahun/on/<int:id>', tahun.On, name='On'),

    # manage user
    path('manage_user/', manage_user.index, name='manage_user'),
    path('manage_user/tambah/', manage_user.addUser, name='add_user'),

    #master lokasi
    path('master_lokasi/', lokasi.index, name='lokasi'),
    path('master_lokasi/tambah/', lokasi.create, name='tambah'),
    path('master_lokasi/hapus/<int:id>', lokasi.permanentDelete, name='hapus'),  
    path('master_lokasi/edit/<int:id>', lokasi.edit, name='edit'),  

    # master_jabatan
    path('master_jabatan/', jabatan.index, name='jabatan'),
    path('master_jabatan/tambah/', jabatan.create, name='tambah'),
    path('master_jabatan/hapus/<int:id>', jabatan.permanentDelete, name='hapus'),
    path('master_jabatan/edit/<int:id>', jabatan.edit, name='edit'),
    path('master_jabatan/off/<int:id>', jabatan.Off, name='Off'),
    path('master_jabatan/on/<int:id>', jabatan.On, name='On'),
 
    # master_pegawai
    path('master_pegawai/', pegawai.index, name='pegawai'),
    path('master_pegawai/tambah/', pegawai.create, name='tambah'),
    path('master_pegawai/hapus/<int:id>', pegawai.permanentDelete, name='hapus'),  
    path('master_pegawai/edit/<int:id>', pegawai.edit, name='edit'), 

    # pengesah
    path('master_pengesah/', pengesah.index, name='pengesah'),
    path('master_pengesah/tambah/', pengesah.create, name='tambah'),
    path('master_pengesah/hapus/<int:id>', pengesah.permanentDelete, name='hapus'),  
    path('master_pengesah/edit/<int:id>', pengesah.edit, name='edit'),
    path('master_pengesah/off/<int:id>', pengesah.Off, name='Off'),
    path('master_pengesah/on/<int:id>', pengesah.On, name='On'),
    
    # master_organisasi
    path('master_organisasi/', organisasi.index, name='organisasi'),
    path('master_organisasi/tambah/', organisasi.create, name='tambah'),
    path('master_organisasi/edit/<int:id>', organisasi.edit, name='edit'),
    path('master_organisasi/hapus/<int:id>', organisasi.permanentDelete, name='hapus'),

    #master ssh luar daerah
    path('master_sshluar/', sshLuar.index, name='sshluar'),
    path('master_sshluar/tambah/', sshLuar.create, name='tambah'),
    path('master_sshluar/edit/<int:id>', sshLuar.edit, name='edit'),
    path('master_sshluar/hapus/<int:id>', sshLuar.permanentDelete, name='hapus'),
    # path('master_jabatan/edit/<int:id>', jabatan.edit, name='edit'),
    # path('master_jabatan/off/<int:id>', jabatan.Off, name='Off'),
    # path('master_jabatan/on/<int:id>', jabatan.On, name='On'),

    #master ssh Dalam daerah
    path('master_sshdalam/', sshDalam.index, name='sshdalam'),
    path('master_sshdalam/tambah/', sshDalam.create, name='tambah'),
    path('master_sshdalam/edit/<int:id>', sshDalam.edit, name='edit'),
    path('master_sshdalam/hapus/<int:id>', sshDalam.permanentDelete, name='hapus'),

    # master_kegiatan
    path('master_kegiatan/', kegiatan.index, name='kegiatan'),
    path('master_kegiatan/tambah/', kegiatan.create, name='tambah'),
    path('master_kegiatan/hapus/<int:id>', kegiatan.permanentDelete, name='hapus'),  
    path('master_kegiatan/edit/<int:id>', kegiatan.edit, name='edit'),


    #Laporan
    #biaya rill
    path('biayarill/', biayarill.index, name='biayarill'),
    #cotsheet
    path('costsheet/', costsheet.index, name='costsheet'),
    #lembar pengesahan
    path('lembar_pengesahan/', lembar_pengesahan.index, name='lembar_pengesahan'),
    # Rincian biaya rampung
    path('rincian_biayarampung/', rincian_biayarampung.index, name='rincian_biayarampung'),
    # SPKD
    path('SPKD/', SPKD.index, name='SPKD'),
    # SPPD Rampung
    path('SPPD_rampung/', SPPD_rampung.index, name='SPPD_rampung'),
    # SPTM
    path('SPTM/', SPTM.index, name='SPTM'),
    
    # SPT
    # path('create_spt/', spt.index, name='create_spt'),
    # path('create_spt_luar/', spt.LuarDaerah, name='create_spt_luar'),
  
    
    
    
    #SPT
    path('list_spt/', spt.index, name='list_spt'),
    path('create_spt/', spt.create_spt, name='create_spt'),
    path('preview/<int:id_spt>', spt.preview, name='preview'),
    path('list_spt/print/<int:id_spt>', spt.print_spt, name='print'),
    path('create_spt/tambah/', spt.create, name='create'),
    path('list_spt/hapus/<int:id>', spt.permanentDelete, name='hapus'), 
    path('list_spt/edit/<int:id_spt>', spt.edit, name='edit_spt'), 
    # path('create_spt/additem', spt.AddPegawai, name='additem'),

    #VERIFIKASI SPT
    path('verifikasi_spt/', verifikasi_spt.index, name='verifikasi_spt'),
    path('verifikasi_spt/approve/<int:id>', verifikasi_spt.Approve, name='approve'),
    path('verifikasi_spt/notapprove/<int:id>', verifikasi_spt.NotApprove, name='notapprove'),
    path('verifikasi_spt/preview/<int:id_spt>', spt.preview, name='preview_verif'),

    # # SPT
    # path('list_spt/', sppd_.index, name='list_spt'),
    # path('list_spt/tambah/', spt.create, name='tambah'),
    # path('list_spt/hapus/<int:id>', spt.permanentDelete, name='hapus'),
    # path('list_spt/approve/<int:id>', spt.approve, name='approve'),
    # path('list_spt/notapprove/<int:id>', spt.NotApprovre, name='notapprove'),

    # SPPD
    path('create_sppd/', sppd_.index, name='create_sppd'),

    path('create_sppd_dalam/', sppd_.idx_dalam, name='create_sppd_dalam'),

    path('create_sppd/tambah/simpan/', sppd_.create, name='save'),
    path('create_sppd/tambah/<int:id>', sppd_.LuarDaerah, name='tambah'),

    path('create_sppd/add/<int:id>', sppd_.DalamDaerah, name='add'),
    path('create_sppd/add/simpan/', sppd_.create_lokal, name='simpan'),

    path('create_sppd/preview/<int:id_sppd>', sppd_.preview, name='view'),
    path('create_sppd/print/<int:id_sppd>', sppd_.print_sppd, name='printsppd'),
    path('create_sppd_luar/<int:id>', sppd_.LuarDaerah, name='create_sppd_luar'),
    path('create_sppd_dalam/<int:id>', sppd_.DalamDaerah, name='create_sppd_dalam'),

    #SPJ PERJADIN
    path('spj_perjadin/', spj_perjadin.index, name='spj_perjadin'),
    path('spj_perjadin/tambah/<int:id>', spj_perjadin.LuarDaerah, name='create_spj'),
    path('spj_perjadin/tambah/simpan/', spj_perjadin.create, name='save_spj'),
    path('spj_perjadin/print/<int:id_sppd>', spj_perjadin.print_spj, name='printspj'),
    path('spj_perjadin/preview/<int:id_sppd>', spj_perjadin.preview_spj, name='preview_spj'),
    path('download_file<int:file_id>/', spj_perjadin.download_file, name='download_file'),




    # path('/create_sppd/add_pegawai/<int:pegawai_id>/', spt.AddPegawai, name='addpegawai'),
    # path('add_pegawai/<int:pegawai_id>/', sppd_.AddPegawai, name='add_pegawai'),

    #lembar pengesahan
    path('lembar_pengesahan/', lembar_pengesahan.index, name='lembar_pengesahan'),
    
    #Rekap Laporan
    # path('rekap_spt/', rekap_laporan.index, name='rekap_spt'),
    # path('rekap_pegawai/', rekap_laporan.Pegawai, name='rekap_pegawai'),
    # path('rekap_sppd/', rekap_laporan.rekap_sppd, name='rekap_sppd'),
    
    #Laporan Rekap SPPD 
    path('laporan/', rekap_laporan.index, name='laporan'),
    path('laporan/print/<int:id>', rekap_laporan.printsppd, name='printsppd'),
    #Laporan  Data Pegawai
    path('data_pegawai/', rekap_laporan.rekap_pegawai, name='rekap_pegawai'),
    path('data_pegawai/print/<int:id>', rekap_laporan.printdata, name='printdata'),
    #Laporan  Rekap SPT
    path('rekap_spt', rekap_laporan.rekap_spt, name='rekap'),
    path('print/<int:id>', rekap_laporan.printspt, name='printspt'),
    #laporan Lembar Pengesah
    path('rekap_pengesah', rekap_laporan.rekap_pengesah, name='rekap_pengesah'),
    path('print/', rekap_laporan.print_pengesah, name='print_pengesah'),
    # path('lembar', rekap_laporan.lembar_pengesah, name='laporan_lembarpengesah'),

    # Biaya rill
    path('biaya_rill', biayarill.index, name='biayarill'),
    path('biaya_rill/print/<int:id>', biayarill.print_biaya, name='print_biaya'),

    # Costsheet
    path('costsheet', costsheet.index, name='costsheet'),
    path('costsheet/print/<int:id>', costsheet.print_costsheet, name='print_costsheet'),

    # kwitansi
    path('kwitansi/<int:id>', kwitansi.print_kwitansi, name='kwitansi'),
    # path('costsheet/print/<int:id>', costsheet.print_costsheet, name='print_costsheet'),

    
    
    
    
]