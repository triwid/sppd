# Generated by Django 4.2.5 on 2023-11-08 01:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sppd', '0029_detailspj_pegawai'),
    ]

    operations = [
        migrations.AddField(
            model_name='detailspj',
            name='pengajuan',
            field=models.IntegerField(default=0),
        ),
    ]
