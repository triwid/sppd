from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import Sppd
from ..models import MasterPengesah
from ..models import MasterPegawai
from ..models import Spt
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.utils.formats import date_format
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *


@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'Rekap Surat SPPD'
    orgid = request.user.id_organisasi_id
    if request.user.hak_akses == 'Admin' :
        sppd = Sppd.objects.all().order_by('id')
    else:
        sppd = Sppd.objects.filter( no_spt_id__kd_organisasi_id = orgid ).order_by('id')
    pengesah = MasterPengesah.objects.all().order_by('id')
    contex = {
        'title' : title,
        'sppd' : sppd,
        'pengesah' : pengesah
        
    }
    return render(request, 'rekap_laporan/rekap_sppd.html', contex)

@login_required
@is_verified()
def rekap_pegawai(request):
    title = 'Rekap Data Pegawai'
    pegawai = MasterPegawai.objects.all().order_by('id')
    pengesah = MasterPengesah.objects.all().order_by('id')
    contex = {
        'title' : title,
        'pegawai' : pegawai,
        'pengesah' : pengesah
        
    }
    return render(request, 'rekap_laporan/rekap_pegawai.html', contex)

@login_required
@is_verified()
def rekap_spt(request):
    title = 'Rekap Data Pegawai'
    orgid = request.user.id_organisasi_id
    pengesah = MasterPengesah.objects.all()
    if request.user.hak_akses == 'Admin' :
        spt = Spt.objects.prefetch_related('detailspt_set', 'sppd_set' ).filter(status=1).order_by('id')
    else:
        spt = Spt.objects.prefetch_related('detailspt_set', 'sppd_set').filter( kd_organisasi_id = orgid, status=1 ).order_by('id')

    pengesah = MasterPengesah.objects.all().order_by('id')
    contex = {
        'title' : title,
        'spt' : spt,
        'pengesah' : pengesah
        
    }
    return render(request, 'rekap_laporan/rekap_spt.html', contex)

@login_required
@is_verified()
def rekap_pengesah(request):
    title = 'Rekap Lembar Pengesah'
    orgid = request.user.id_organisasi_id
    pengesah = MasterPengesah.objects.all().order_by('id')
    if request.user.hak_akses == 'Admin' :
        spj = Spj.objects.all().prefetch_related('detailspj_set').order_by('id')
    else:
        spj = Spj.objects.all().prefetch_related('detailspj_set').filter( no_sppd_id__no_spt_id__kd_organisasi_id = orgid).order_by('id')
    

    data_sshdalam = []

    for data in spj:
        id_pegawai_totals = {}  # Dictionary untuk menyimpan total realisasi, sisa, dan pengajuan per ID pegawai
            
        for detail in data.detailspj_set.all():
                # Mendapatkan ID pegawai dari detail
            id_pegawai = detail.pegawai.id
                
                # Memeriksa apakah ID pegawai sudah ada di dictionary
            if id_pegawai in id_pegawai_totals:
                    # Jika sudah ada, tambahkan nilai dari detail ke total yang sudah ada
                id_pegawai_totals[id_pegawai]['realisasi'] += detail.realisasi
                id_pegawai_totals[id_pegawai]['sisa'] += detail.sisa
                id_pegawai_totals[id_pegawai]['pengajuan'] += detail.pengajuan
            else:
                    # Jika belum ada, buat entri baru dengan nilai dari detail
                id_pegawai_totals[id_pegawai] = {
                    'realisasi': detail.realisasi,
                    'sisa': detail.sisa,
                    'pengajuan': detail.pengajuan,
                    'nama': detail.pegawai.nama,  # Tambahkan nama di sini
                    'no_sppd': data.no_sppd.no_sppd,  # Tambahkan no_sppd di sini
                    'tanggal': data.tanggal,  # Tambahkan tanggal di sini
                }
                
            
            # Setelah loop detail, tambahkan data total per ID pegawai ke dalam data_sshdalam
        for id_pegawai, totals in id_pegawai_totals.items():
            dict_sshdalam = {
                'id': id_pegawai,
                'nama': totals['nama'],
                'no_sppd': totals['no_sppd'],
                'tanggal': totals['tanggal'],
                'total_realisasi': totals['realisasi'],
                'total_sisa': totals['sisa'],
                'total_pengajuan': totals['pengajuan'],
            }
            data_sshdalam.append(dict_sshdalam)

    total_realisasi = 0
    total_sisa = 0
    total_pengajuan = 0

    for item in data_sshdalam:
        total_realisasi += item['total_realisasi']
        total_sisa += item['total_sisa']
        total_pengajuan += item['total_pengajuan']

    print(data_sshdalam)

    context = {
        'title': title,
        'data_sshdalam': data_sshdalam,
        'pengesah': pengesah,
        'total_realisasi': total_realisasi,
        'total_sisa': total_sisa,
        'total_pengajuan': total_pengajuan,
    }
    return render(request, 'rekap_laporan/rekap_pengesah.html', context) 

@login_required
@is_verified()
def printsppd(request,id):
    if request.method == 'POST':
        title = 'Print SPPD'
        orgid = request.user.id_organisasi_id
        if request.user.hak_akses == 'Admin' :
            sppd = Sppd.objects.all().prefetch_related('detailsppd_set').order_by('id')
        else:
            sppd = Sppd.objects.prefetch_related('detailsppd_set').filter( no_spt_id__kd_organisasi_id = orgid ).order_by('id')

        nama = request.POST.get('pengesah')
        pengesah = MasterPegawai.objects.filter(nama=nama)
        tanggal_input = request.POST.get('tanggal')
        tanggal = datetime.strptime(tanggal_input, '%Y-%m-%d').date()
        tanggal_format = date_format(tanggal, format='j F Y', use_l10n=True)
        context = {
            'title': title,
            'sppd': sppd,
            'nama': nama,
            'tanggal': tanggal_format,
            'pengesah': pengesah,
        
    
        }
        return render(request, 'rekap_laporan/print_laporan.html', context)

@login_required
@is_verified()   
def printdata(request,id):
        
        if request.method == 'POST':
             title = 'Print Data Pegawai'
             pegawai = MasterPegawai.objects.all()
             nama = request.POST.get('pengesah')
             pengesah = MasterPegawai.objects.filter(nama=nama)
             tanggal_input = request.POST.get('tanggal')
             tanggal = datetime.strptime(tanggal_input, '%Y-%m-%d').date()
             tanggal_format = date_format(tanggal, format='j F Y', use_l10n=True)
             context = {
            'title': title,
            'pegawai': pegawai,     
            'nama': nama,
            'tanggal': tanggal_format,
            'pengesah': pengesah,

        }
        return render(request, 'rekap_laporan/print_data.html', context)

@login_required
@is_verified()   
def printspt(request,id):
        
        if request.method == 'POST':
             title = 'Print Spt'
             orgid = request.user.id_organisasi_id
             if request.user.hak_akses == 'Admin' :
                spt = Spt.objects.prefetch_related('detailspt_set', 'sppd_set' ).filter(status=1).order_by('id')
             else:
                spt = Spt.objects.prefetch_related('detailspt_set', 'sppd_set').filter( kd_organisasi_id = orgid, status=1 ).order_by('id')

             nama = request.POST.get('pengesah')
             pengesah = MasterPegawai.objects.filter(nama=nama)
             tanggal_input = request.POST.get('tanggal')
             tanggal = datetime.strptime(tanggal_input, '%Y-%m-%d').date()
             tanggal_format = date_format(tanggal, format='j F Y', use_l10n=True)
             context = {
            'title': title,
            'spt': spt,     
            'nama': nama,
            'tanggal': tanggal_format,
            'pengesah': pengesah,

        }
        return render(request, 'rekap_laporan/print_spt.html', context)

# def printpengesah(request,id):
        
#         if request.method == 'POST':
#              title = 'Print Spt'
#              spt = Spt.objects.prefetch_related('detailspt_set', 'sppd_set' ).filter(status=1)
#              nama = request.POST.get('pengesah')
#              pengesah = MasterPegawai.objects.filter(nama=nama)
#              tanggal_input = request.POST.get('tanggal')
#              tanggal = datetime.strptime(tanggal_input, '%Y-%m-%d').date()
#              tanggal_format = date_format(tanggal, format='j F Y', use_l10n=True)
#              context = {
#             'title': title,
#             'spt': spt,     
#             'nama': nama,
#             'tanggal': tanggal_format,
#             'pengesah': pengesah,

#         }
#         return render(request, 'rekap_laporan/print_spt.html', context)

@login_required
@is_verified()    
def print_pengesah(request):
    if request.method == 'POST':
        title = 'Print SPPD'
        orgid = request.user.id_organisasi_id
        if request.user.hak_akses == 'Admin' :
            spj = Spj.objects.all().prefetch_related('detailspj_set').order_by('id')
        else:
            spj = Spj.objects.all().prefetch_related('detailspj_set').filter( no_sppd_id__no_spt_id__kd_organisasi_id = orgid).order_by('id')
        nama = request.POST.get('pengesah')
        pengesah = MasterPengesah.objects.filter(pegawai_id__nama=nama).all()
        tanggal_input = request.POST.get('tanggal')
        tanggal = datetime.strptime(tanggal_input, '%Y-%m-%d').date()
        tanggal_format = date_format(tanggal, format='j F Y', use_l10n=True)

        data_sshdalam = []

        for data in spj:
            id_pegawai_totals = {}  # Dictionary untuk menyimpan total realisasi, sisa, dan pengajuan per ID pegawai
            
            for detail in data.detailspj_set.all():
                # Mendapatkan ID pegawai dari detail
                id_pegawai = detail.pegawai.id
                
                # Memeriksa apakah ID pegawai sudah ada di dictionary
                if id_pegawai in id_pegawai_totals:
                    # Jika sudah ada, tambahkan nilai dari detail ke total yang sudah ada
                    id_pegawai_totals[id_pegawai]['realisasi'] += detail.realisasi
                    id_pegawai_totals[id_pegawai]['sisa'] += detail.sisa
                    id_pegawai_totals[id_pegawai]['pengajuan'] += detail.pengajuan
                else:
                    # Jika belum ada, buat entri baru dengan nilai dari detail
                    id_pegawai_totals[id_pegawai] = {
                        'realisasi': detail.realisasi,
                        'sisa': detail.sisa,
                        'pengajuan': detail.pengajuan,
                        'nama': detail.pegawai.nama,  # Tambahkan nama di sini
                        'no_sppd': data.no_sppd.no_sppd,  # Tambahkan no_sppd di sini
                        'tanggal': data.tanggal,  # Tambahkan tanggal di sini
                    }
                
            
            # Setelah loop detail, tambahkan data total per ID pegawai ke dalam data_sshdalam
            for id_pegawai, totals in id_pegawai_totals.items():
                dict_sshdalam = {
                    'id': id_pegawai,
                    'nama': totals['nama'],
                    'no_sppd': totals['no_sppd'],
                    'tanggal': totals['tanggal'],
                    'total_realisasi': totals['realisasi'],
                    'total_sisa': totals['sisa'],
                    'total_pengajuan': totals['pengajuan'],
                }
                data_sshdalam.append(dict_sshdalam)

        total_realisasi = 0
        total_sisa = 0
        total_pengajuan = 0

        for item in data_sshdalam:
            total_realisasi += item['total_realisasi']
            total_sisa += item['total_sisa']
            total_pengajuan += item['total_pengajuan']

        print(data_sshdalam)

        context = {
            'title': title,
            'data_sshdalam': data_sshdalam,
            'nama': nama,
            'pengesah': pengesah,
            'tanggal': tanggal_format,
            'total_realisasi': total_realisasi,
            'total_sisa': total_sisa,
            'total_pengajuan': total_pengajuan,
        }
        return render(request, 'laporan/laporan/pengesah.html', context) 
    
  
    
