from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import SshLuarDaerah
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *


@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'SshLuarDaerah'
    page = request.GET.get('page', 1)
    sshLuar = SshLuarDaerah.objects.all().order_by('id').values()
    paginator = Paginator(sshLuar, 200)
    try:
        sshLuar = paginator.page(page)
    except PageNotAnInteger:
        sshLuar = paginator.page(1)
    except EmptyPage:
        sshLuar = paginator.page(paginator.num_pages)
    contex = {
        'title' : title,
        'sshLuar' : sshLuar
    }
    return render(request, 'master/master_ssh/ssh_luar/index.html', contex)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = SshLuarDaerah.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except SshLuarDaerah.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def create(request):
    if request.method == 'POST':
        insert = SshLuarDaerah()
        jenis = request.POST.get('jenis')
        tipe = request.POST.get('tipe')
        satuan = request.POST.get('satuan')
        uraian = request.POST.get('uraian')
        harga = request.POST.get('harga')
        type_transportasi = request.POST.get('type_tras')
        keterangan = request.POST.get('ket')
        if insert is not None:
            insert.jenis = jenis
            insert.tipe = tipe
            insert.satuan = satuan
            insert.uraian = uraian
            insert.harga = harga
            insert.type_transportasi = type_transportasi
            insert.keterangan = keterangan
            insert.save()
            
            messages.success(request, 'Kata Mereka berhasil disimpan.')
            return redirect('sppd:sshluar')

@login_required
@is_verified()
def edit(request, id):
    if request.method == 'POST':
        edit = SshLuarDaerah.objects.get(id = id)
        jenis = request.POST['jnsEdit']
        tipe = request.POST['tpEdit']
        satuan = request.POST['stnEdit']
        uraian = request.POST['urnEdit']
        harga = request.POST['hrgEdit']
        type_transportasi = request.POST['tpTrans']
        keterangan = request.POST['ketEdit']
        if edit is not None:
                edit.jenis = jenis
                edit.tipe = tipe
                edit.satuan = satuan
                edit.uraian = uraian
                edit.harga = harga
                edit.type_transportasi = type_transportasi
                edit.keterangan = keterangan
                edit.save()

                messages.success(request, 'Kata Mereka berhasil disimpan.')
                return redirect('sppd:sshluar')