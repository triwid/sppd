from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import MasterPegawai
from ..models import MasterJabatan
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *


@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'MasterPegawai'
    page = request.GET.get('page', 1)
    pegawai = MasterPegawai.objects.all()
    jabatan = MasterJabatan.objects.all().order_by('id').values()
    paginator = Paginator(pegawai, 200)
    try:
        pegawai = paginator.page(page)
    except PageNotAnInteger:
        pegawai = paginator.page(1)
    except EmptyPage:
        pegawai = paginator.page(paginator.num_pages)
    contex = {
        'title' : title,
        'pegawai' : pegawai,
        'jabatan' : jabatan, 
    }
    return render(request, 'master/master_pegawai/index.html', contex)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = MasterPegawai.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except MasterPegawai.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def create(request):
    if request.method == 'POST':
        insert = MasterPegawai()
        nip = request.POST.get('nip')
        nama = request.POST.get('nama')
        id_jabatan = request.POST.get('jabatan')
        golongan = request.POST.get('golongan')
        no_hp = request.POST.get('nohp')
        eselon = request.POST.get('eselon')
        alamat = request.POST.get('alamat')
        pangkat = request.POST.get('pangkat')
        email = request.POST.get('email')

        if insert is not None:
            insert.nip = nip
            insert.nama = nama
            insert.jabatan_id = id_jabatan
            insert.golongan = golongan
            insert.no_hp = no_hp
            insert.eselon = eselon
            insert.alamat = alamat
            insert.pangkat = pangkat
            insert.email = email
            

        insert.save()
            
        messages.success(request, 'Kata Mereka berhasil disimpan.')
        return redirect('sppd:pegawai')

@login_required
@is_verified()
def edit(request, id):
    if request.method == 'POST':
        edit = MasterPegawai.objects.get(id = id)
        nip = request.POST.get('nipedit')
        nama = request.POST.get('namaedit')
        id_jabatan = request.POST.get('jabatan')
        golongan = request.POST.get('golonganedit')
        no_hp = request.POST.get('nohpedit')
        eselon = request.POST.get('eselonedit')
        alamat = request.POST.get('alamatedit')
        pangkat = request.POST.get('pangkatedit')
        email = request.POST.get('emailedit')
        jabatan_id = request.POST.get('jabatan')
        
        if edit is not None:
            try:
                edit.nip = nip
                edit.nama = nama
                edit.no_hp = no_hp
                edit.golongan = golongan
                edit.eselon = eselon
                edit.alamat = alamat
                edit.pangkat = pangkat
                edit.email = email
                edit.jabatan_id = id_jabatan
                edit.save()

                messages.success(request, 'Kata Mereka berhasil disimpan.')
                return redirect('sppd:pegawai')
            except Exception as e:
                messages.success(request, f'Terjadi kesalahan, {str(e)}')
                return redirect('sppd:pegawai')

@login_required
@is_verified()
def Off(request, id=""):
    MasterPegawai.objects.filter(id=id).update(status = 0)
    messages.success(request, 'Kata Mereka berhasil disimpan.')
    return redirect('sppd:pegawai')

@login_required
@is_verified()
def On(request, id=""):
    MasterPegawai.objects.filter(id=id).update(status = 1)
    messages.success(request, 'Kata Mereka berhasil disimpan.')
    return redirect('sppd:pegawai')