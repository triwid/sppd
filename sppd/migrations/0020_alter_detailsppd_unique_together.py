# Generated by Django 4.2.4 on 2023-10-20 08:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sppd', '0019_sppd_detailsppd'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='detailsppd',
            unique_together={('no_sppd', 'pegawai', 'uraian_ssh')},
        ),
    ]
