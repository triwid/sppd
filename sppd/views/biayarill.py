from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *
from ..models import Sppd, MasterPengesah
from collections import defaultdict
from datetime import datetime
from django.utils.formats import date_format

# # Create your views here.
@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'Biaya Rill'
    sppd = Sppd.objects.prefetch_related('spj_set').order_by('id')
    pengesah = MasterPengesah.objects.all().order_by('id')
    contex = {
        'title' : title,
        'sppd' : sppd,
        'pengesah' : pengesah,
    
    }
    
    return render(request, 'rekap_laporan/biaya_rill.html', contex)

@login_required
@is_verified()
def print_biaya(request, id):
    title = 'Biaya Rill'
    sppd_list = Sppd.objects.filter(id=id).prefetch_related('detailsppd_set')
    nama = request.POST.get('pengesah')
    pengesah = MasterPengesah.objects.filter(pegawai_id = nama).all()
    tanggal_input = request.POST.get('tanggal')
    tanggal = datetime.strptime(tanggal_input, '%Y-%m-%d').date()
    tanggal_format = date_format(tanggal, format='j F Y', use_l10n=True)

    data_sppd_list = []
    nama_detail_pegawai_dict = defaultdict(list)
    total_per_pegawai = defaultdict(float)

    for sppd in sppd_list:
        tanggal = sppd.tgl_sppd
        no_sppd = sppd.no_sppd

        for detail_pegawai in sppd.detailsppd_set.all():
            subtotal = detail_pegawai.subtotal

            data_sppd = {
                'uraian_ssh': detail_pegawai.uraian_ssh,
                'subtotal': subtotal,
                'nama': detail_pegawai.pegawai.nama,
                'nip': detail_pegawai.pegawai.nip,
                'jabatan': detail_pegawai.pegawai.jabatan.jabatan,
            }

            data_sppd_list.append(data_sppd)
            nama_detail_pegawai_dict[detail_pegawai.pegawai.nama].append(data_sppd)
            total_per_pegawai[detail_pegawai.pegawai.nama] += subtotal

    nama_detail_pegawai_list = list(nama_detail_pegawai_dict.items())

    for nama, total in total_per_pegawai.items():
        # Update the 'total' field in each data_sppd entry for the corresponding employee
        for data_sppd in nama_detail_pegawai_dict[nama]:
            data_sppd['total'] = '{:.2f}'.format(total)

    print(nama_detail_pegawai_list, pengesah)
    context = {
        'title': title,
        'dt_pegawai': data_sppd_list,
        'nama_detail_pegawai_list': nama_detail_pegawai_list,
        'tanggal': tanggal,
        'no_sppd': no_sppd,
        'tgl_pengesah': tanggal_format,
        'pengesah': pengesah,
    }

    return render(request, 'laporan/laporan/biayarill.html', context)


