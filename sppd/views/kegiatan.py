from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import MasterKegiatan
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *

# # Create your views here.
@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'Master Kegiatan'
    page = request.GET.get('page', 1)
    kegiatan = MasterKegiatan.objects.all().order_by('id').values()
    paginator = Paginator(kegiatan, 200)
    try:
        kegiatan = paginator.page(page)
    except PageNotAnInteger:
        kegiatan = paginator.page(1)
    except EmptyPage:
        kegiatan = paginator.page(paginator.num_pages)
    contex = {
        'title' : title,
        'data_kegiatan' : kegiatan
    }
    return render(request, 'master/master_kegiatan/index.html', contex)

@login_required
@is_verified()
def create(request):
    if request.method == 'POST':
        insert = MasterKegiatan()
        kd_bidang = request.POST.get('kd_bidang')
        urai_bidang = request.POST.get('urai_bidang')
        kd_program = request.POST.get('kd_program')
        urai_program = request.POST.get('urai_program')
        kd_kegiatan = request.POST.get('kd_kegiatan')
        urai_kegiatan = request.POST.get('urai_kegiatan')
        kd_subkegiatan = request.POST.get('kd_subkegiatan')
        urai_subkegiatan = request.POST.get('urai_subkegiatan')
        pagu = request.POST.get('pagu')
        pagu_p = request.POST.get('pagu_p')
        sumber_dana = request.POST.get('sumber_dana')

        if insert is not None:
            insert.kd_bidang = kd_bidang
            insert.urai_bidang = urai_bidang
            insert.kd_program = kd_program
            insert.urai_program = urai_program
            insert.kd_kegiatan = kd_kegiatan
            insert.urai_kegiatan = urai_kegiatan
            insert.kd_subkegiatan = kd_subkegiatan
            insert.urai_subkegiatan = urai_subkegiatan
            insert.pagu = pagu
            insert.pagu_p = pagu_p
            insert.sumber_dana = sumber_dana
            
            insert.save()
            messages.success(request, 'Data Berhasil disimpan.')
            return redirect('sppd:kegiatan')


@login_required
@is_verified()       
def permanentDelete(request, id):
    message = ''
    try:
        doc = MasterKegiatan.objects.get(id=id)
        doc.delete()
        message = 'success'
    except MasterKegiatan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def edit(request, id):
    if request.method == 'POST':
        edit = MasterKegiatan.objects.get(id = id)
        kd_bidang = request.POST.get('kd_bidang')
        urai_bidang = request.POST.get('urai_bidang')
        kd_program = request.POST.get('kd_program')
        urai_program = request.POST.get('urai_program')
        kd_kegiatan = request.POST.get('kd_kegiatan')
        urai_kegiatan = request.POST.get('urai_kegiatan')
        kd_subkegiatan = request.POST.get('kd_subkegiatan')
        urai_subkegiatan = request.POST.get('urai_subkegiatan')
        pagu = request.POST.get('pagu')
        pagu_p = request.POST.get('pagu_p')
        sumber_dana = request.POST.get('sumber_dana')
        
        if edit is not None:
                edit.kd_bidang = kd_bidang
                edit.urai_bidang = urai_bidang
                edit.kd_program = kd_program
                edit.urai_program = urai_program
                edit.kd_kegiatan = kd_kegiatan
                edit.urai_kegiatan = urai_kegiatan
                edit.kd_subkegiatan = kd_subkegiatan
                edit.urai_subkegiatan = urai_subkegiatan
                edit.pagu = pagu
                edit.pagu_p = pagu_p
                edit.sumber_dana = sumber_dana
                edit.save()

                messages.success(request, 'Data Berhasil Diubah.')
                return redirect('sppd:kegiatan')

# def Off(request, id=""):
#     MasterJabatan.objects.filter(id=id).update(status = 0)
#     messages.success(request, 'Kata Mereka berhasil disimpan.')
#     return redirect('sppd:jabatan')

# def On(request, id=""):
#     MasterJabatan.objects.filter(id=id).update(status = 1)
#     messages.success(request, 'Kata Mereka berhasil disimpan.')
#     return redirect('sppd:jabatan')