from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *
from django.utils.formats import date_format
from ..models import MasterOrganisasi
from ..models import MasterLokasi
from ..models import MasterKegiatan
from ..models import MasterPegawai
from ..models import SshLuarDaerah
from ..models import MasterPengesah
from ..models import Spt
from ..models import DetailSpt
from ..models import Sppd
from ..models import Spj
from ..models import DetailSppd
from django.db import IntegrityError
import pprint
from django.db.models import Case, When, Q
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..decorators import *

# # Create your views here.

@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    title = 'Membuat SPPD'
    orgid = request.user.id_organisasi_id
    if request.user.hak_akses == 'Admin' :
        spt = Spt.objects.prefetch_related('detailspt_set', 'sppd_set' ).filter(status=1).order_by('-id')
    else:
        spt = Spt.objects.prefetch_related('detailspt_set', 'sppd_set').filter( kd_organisasi_id = orgid, status=1 ).order_by('-id')
    pengesah = MasterPengesah.objects.all()
    contex = {
        'title' : title,
        'spt' : spt,
        'pengesah': pengesah,
    }
    return render(request, 'SPPD/index.html', contex)

@login_required
@is_verified()
@require_http_methods(["GET"])
def idx_dalam(request):
    title = 'Membuat SPPD'
    orgid = request.user.id_organisasi_id
    if request.user.hak_akses == 'Admin' :
        spt = Spt.objects.prefetch_related('detailspt_set', 'sppd_set' ).filter(status=1).order_by('-id')
    else:
        spt = Spt.objects.prefetch_related('detailspt_set', 'sppd_set').filter( kd_organisasi_id = orgid, status=1 ).order_by('-id')
    pengesah = MasterPengesah.objects.all()
    contex = {
        'title' : title,
        'spt' : spt,
        'pengesah': pengesah,
    }
    return render(request, 'SPPD/idx_dalam_daerah.html', contex)

@login_required
@is_verified()
def DalamDaerah(request, id):
    title = 'Membuat SPPD'
    organisasi= MasterOrganisasi.objects.all().order_by('id_organisasi').values()
    lokasi = MasterLokasi.objects.all().order_by('id').values()
    kegiatan = MasterKegiatan.objects.all().order_by('id').values()
    pegawai = MasterPegawai.objects.filter(id=id)
    ssh_dalam_daerah = SshDalamDaerah.objects

    no_sppd = request.POST.get('nosppd')
    spt = Spt.objects.prefetch_related('detailspt_set').filter(id=id).all()

    data_sshlokal = []
    for detail_spt in spt:
        for data in detail_spt.detailspt_set.all():
            array_ssh = []
            dict_sshlokal = {}
            # arr_1 = []
            for x in ssh_dalam_daerah.filter(zona=detail_spt.status_perjalanan):
                dict_sshlokal_ = {}
                
                if x.uraian == data.pegawai.jabatan.jabatan or x.uraian == data.pegawai.eselon:
                    #     print(x.)
                    dict_sshlokal_ = {
                            'id': data.pegawai.id,
                            'nip': data.pegawai.nip,
                            'nama': data.pegawai.nama,
                            'tipe': x.tipe,
                            'satuan': x.satuan,
                            'harga': x.harga,
                            'volume': 0,
                            'jabatan': data.pegawai.jabatan,          
                            'eselon': data.pegawai.eselon,
                    }
                    if x.tipe == "Uang Harian/Lumpsum":
                        dict_sshlokal_['volume'] = 5
                        dict_sshlokal_['subtotal'] = dict_sshlokal_['volume'] * dict_sshlokal_['harga']
                    array_ssh.append(dict_sshlokal_)
                    
            dict_sshlokal['nama'] = data.pegawai.nama
            dict_sshlokal['id'] = data.pegawai.id
            dict_sshlokal['nip'] = data.pegawai.nip
            dict_sshlokal['detail'] = array_ssh
            data_sshlokal.append(dict_sshlokal)
            total = [y['subtotal'] for x in data_sshlokal for y in x['detail']]
            total = sum(total)
  
    contex = {
        'title' : title,
        'organisasi' : organisasi,
        'lokasi' : lokasi,
        'kegiatan' : kegiatan,
        'pegawai' : pegawai,
        'nosppd' : no_sppd,
        'detailspt' : spt,
        'total' : total,
        'data_sshlokal' : data_sshlokal,
    }
    return render(request, 'SPPD/form_dalam.html', contex)


@login_required
@is_verified()
def LuarDaerah(request, id):
    title = 'Membuat SPPD'
    print(request.POST)
    type_tras = request.POST.get('type_tras')
    tipe = request.POST.get('tipe')
    organisasi= MasterOrganisasi.objects.all().order_by('id_organisasi').values()
    lokasi = MasterLokasi.objects.all().order_by('id').values()
    kegiatan = MasterKegiatan.objects.all().order_by('id').values()
    pegawai = MasterPegawai.objects.filter(id=id)
    sshluar = SshLuarDaerah.objects.filter(jenis='Luar Provinsi')
    sshdalam = SshLuarDaerah.objects.filter(Q(id=Case(
                                                    When(Q(tipe='Tiket', type_transportasi__icontains = type_tras), then='id'),
                                                    When(Q(tipe='Transport', keterangan__icontains = tipe), then='id'),
                                                    When(~Q(tipe__in=['Tiket', 'Transport']), then='id'),
                                                    default=None
                                                    )), jenis='Dalam Provinsi')
    # print(sshdalam.query)
    no_sppd = request.POST.get('nosppd')
    spt = Spt.objects.prefetch_related('detailspt_set').filter(id=id).all()

    data_sshdalam = []
    for detail_spt in spt:
        for data in detail_spt.detailspt_set.all():
            array_ssh = []
            dict_sshdalam = {}
            # arr_1 = []
            for x in sshdalam:
                if x.uraian == data.pegawai.jabatan.jabatan or x.uraian == data.pegawai.eselon:
                    # if x.tipe == 'Tiket':
                    #     print(x.)
                    dict_sshdalam_ = {}
                    dict_sshdalam_ = {
                            'id': data.pegawai.id,
                            'nip': data.pegawai.nip,
                            'nama': data.pegawai.nama,
                            'tipe': x.tipe,
                            'satuan': x.satuan,
                            'harga': x.harga,
                            'volume': 0,
                            'jabatan': data.pegawai.jabatan,
                            'eselon': data.pegawai.eselon,
                    }
                    if x.tipe == "Tiket":
                        dict_sshdalam_['volume'] = 1
                        dict_sshdalam_['subtotal'] = dict_sshdalam_['volume'] * dict_sshdalam_['harga']
                    elif x.tipe == "Uang Representatif":
                        dict_sshdalam_['volume'] = 1
                        dict_sshdalam_['subtotal'] = dict_sshdalam_['volume'] * dict_sshdalam_['harga']
                    elif x.tipe == "Transport":
                        dict_sshdalam_['volume'] = 1
                        dict_sshdalam_['subtotal'] = dict_sshdalam_['volume'] * dict_sshdalam_['harga']
                    else:
                        dict_sshdalam_['volume'] = 7
                        dict_sshdalam_['subtotal'] = dict_sshdalam_['volume'] * dict_sshdalam_['harga']
                    array_ssh.append(dict_sshdalam_)
                    
            dict_sshdalam['nama'] = data.pegawai.nama
            dict_sshdalam['id'] = data.pegawai.id
            dict_sshdalam['nip'] = data.pegawai.nip
            dict_sshdalam['detail'] = array_ssh
            data_sshdalam.append(dict_sshdalam)
    
            total = [y['subtotal'] for x in data_sshdalam for y in x['detail']]
            total = sum(total)
    
    data_sshluar = []
    for detail_spt in spt:
        for data in detail_spt.detailspt_set.all():
            array_ssh = []
            dict_sshluar = {}
            # arr_1 = []
            for x in sshluar:
                if x.uraian == data.pegawai.jabatan.jabatan or x.uraian == data.pegawai.eselon:
                    dict_sshluar_ = {}
                    dict_sshluar_ = {
                            'id': data.pegawai.id,
                            'nip': data.pegawai.nip,
                            'nama': data.pegawai.nama,
                            'tipe': x.tipe,
                            'satuan': x.satuan,
                            'harga': x.harga,
                            'volume': 0,
                            'jabatan': data.pegawai.jabatan,
                            'eselon': data.pegawai.eselon,
                    }
                    if x.tipe == "Tiket":
                        dict_sshluar_['volume'] = 1
                        dict_sshluar_['subtotal'] = dict_sshluar_['volume'] * dict_sshluar_['harga']
                    elif x.tipe == "Uang Representatif":
                        dict_sshluar_['volume'] = 1
                        dict_sshluar_['subtotal'] = dict_sshluar_['volume'] * dict_sshluar_['harga']
                    elif x.tipe == "Transport":
                        dict_sshluar_['volume'] = 1
                        dict_sshluar_['subtotal'] = dict_sshluar_['volume'] * dict_sshluar_['harga']
                    else:
                        dict_sshluar_['volume'] = 7
                        dict_sshluar_['subtotal'] = dict_sshluar_['volume'] * dict_sshluar_['harga']
                    array_ssh.append(dict_sshluar_)  
                    
            dict_sshluar['nama'] = data.pegawai.nama
            dict_sshluar['id'] = data.pegawai.id
            dict_sshluar['nip'] = data.pegawai.nip
            dict_sshluar['detail'] = array_ssh
            data_sshluar.append(dict_sshluar)
    
            total = [y['subtotal'] for x in data_sshluar for y in x['detail']]
            total = sum(total)
   
    contex = {
        'title' : title,
        'organisasi' : organisasi,
        'lokasi' : lokasi,
        'kegiatan' : kegiatan,
        'pegawai' : pegawai,
        'sshluar' : sshluar,
        'sshdalam' : sshdalam,
        'detailspt' : spt,
        'data_sshluar':data_sshluar,
        'data_sshdalam':data_sshdalam,
        'total': total,
        'nosppd' : no_sppd,
    }
    return render(request, 'SPPD/form_luar.html', contex)

@login_required
@is_verified()
def create(request):
    if request.method == 'POST':
        try:
            # Ambil data dari form POST
            nosppd = request.POST.get('nosppd')
            nospt = request.POST.get('nospt')
            tgl_sppd = request.POST.get('tanggalsppd')
            total_perjalanan = request.POST.get('total') 
            # Buat objek Spt dan simpan
            sppd = Sppd(
                no_sppd = nosppd,
                no_spt_id = nospt,
                tgl_sppd = tgl_sppd,
                status = 1,
                total_perjalanan = total_perjalanan,
            )
            sppd.save()
            
         # Ambil ID Spt yang baru saja dibuat
            id_sppd = sppd.id

            # Ambil data dari form POST
            list_pegawai = request.POST.getlist('pegawai')
            list_uraian = request.POST.getlist('uraianssh')
            list_satuan = request.POST.getlist('satuan')
            list_volume = request.POST.getlist('volume')
            list_nilai = request.POST.getlist('nilai')
            list_subtotal = request.POST.getlist('subtotal')

            print(list_pegawai)
            print(list_uraian)
            print(list_satuan)
            print(list_nilai)
            print(list_volume)
            print(list_subtotal)
            # Loop melalui data POST yang diterima
            pprint.pprint([(pegawai, uraian, satuan, volume, nilai, subtotal) for pegawai, uraian, satuan, volume, nilai, subtotal in zip(list_pegawai, list_uraian, list_satuan, list_volume, list_nilai, list_subtotal)])
            for pegawai, uraian, satuan, volume, nilai, subtotal in zip(list_pegawai, list_uraian, list_satuan, list_volume, list_nilai, list_subtotal):
                
                detail_sppd = DetailSppd(
                    pegawai_id=pegawai,
                    no_sppd_id=id_sppd,
                    uraian_ssh=uraian,
                    satuan=satuan,
                    volume=volume,
                    nilai=nilai,
                    subtotal=subtotal
                )
                detail_sppd.save()
               
            # Simpan perubahan ke database
            return redirect('sppd:create_sppd')
        except IntegrityError as e:
            print('Data sudah ada')
            messages.error(request, 'Data sudah di buat SPPD')
        except Exception as e:
            print(e)  # Tampilkan pesan kesalahan untuk debug

    return redirect('sppd:create_sppd')  # Gantilah dengan template yang sesuai

@login_required
@is_verified() 
def create_lokal(request):
    if request.method == 'POST':
        try:
            # Ambil data dari form POST
            nosppd = request.POST.get('nosppd')
            nospt = request.POST.get('nospt')
            tgl_sppd = request.POST.get('tanggalsppd')
            total_perjalanan = request.POST.get('total') 
            # Buat objek Spt dan simpan
            sppd = Sppd(
                no_sppd = nosppd,
                no_spt_id = nospt,
                tgl_sppd = tgl_sppd,
                status = 1,
                total_perjalanan = total_perjalanan,
            )
            sppd.save()
            
         # Ambil ID Spt yang baru saja dibuat
            id_sppd = sppd.id
            print(id_sppd)
            # Ambil data dari form POST
            list_pegawai = request.POST.getlist('pegawai')
            list_uraian = request.POST.getlist('uraianssh')
            list_satuan = request.POST.getlist('satuan')
            list_volume = request.POST.getlist('volume')
            list_nilai = request.POST.getlist('nilai')
            list_subtotal = request.POST.getlist('subtotal')

            pprint.pprint([(pegawai, uraian, satuan, volume, nilai, subtotal) for pegawai, uraian, satuan, volume, nilai, subtotal in zip(list_pegawai, list_uraian, list_satuan, list_volume, list_nilai, list_subtotal)])
            for pegawai, uraian, satuan, volume, nilai, subtotal in zip(list_pegawai, list_uraian, list_satuan, list_volume, list_nilai, list_subtotal):
                    
                detail_sppd = DetailSppd(
                    pegawai_id=pegawai,
                    no_sppd_id=id_sppd,
                    uraian_ssh=uraian,
                    satuan=satuan,
                    volume=volume,
                    nilai=nilai,
                    subtotal=subtotal
                )
                detail_sppd.save()
               
            # Simpan perubahan ke database
            return redirect('sppd:create_sppd')
        except IntegrityError as e:
            print('Data sudah ada')
            messages.error(request, 'Data sudah di buat SPPD')
        except Exception as e:
            print(e)  # Tampilkan pesan kesalahan untuk debug

    return redirect('sppd:create_sppd')  # Gantilah dengan template yang sesuai

@login_required
@is_verified()
def preview(request, id_sppd):
    sppd = Sppd.objects.filter(id = id_sppd).prefetch_related('detailsppd_set')
    subtotal = 0
    for x in sppd:
        id_temp = ''
        no = 1
        for item in x.detailsppd_set.all():
            if id_temp == item.pegawai.id:
                item.pegawai.nip = ''
                item.pegawai.nama = ''
                
            else:
                id_temp = item.pegawai.id
                item.no = no
                no += 1
            
            subtotal = subtotal + item.subtotal

    context = {
            'title': 'Data SPPD',
            'detail_spt': sppd,
            'data_sppd' :sppd,
            'total': subtotal,
        }
    return render(request, 'SPPD/preview_sppd.html', context) 

@login_required
@is_verified()
def print_sppd(request, id_sppd):
    if request.method == 'POST':
        title = 'Print SPPD'
        spt = Spt.objects.filter(id = id_sppd).prefetch_related('detailspt_set', 'sppd_set')
        nama = request.POST.get('pengesah')
        pengesah = MasterPengesah.objects.filter(pegawai_id = nama).all()
        tanggal_input = request.POST.get('tanggal')
        tanggal = datetime.strptime(tanggal_input, '%Y-%m-%d').date()
        tanggal_format = date_format(tanggal, format='j F Y', use_l10n=True)
        context = {
            'title': title,
            'spt': spt,
            'nama': nama,
            'pengesah': pengesah,
            'tanggal': tanggal_format,
        }
        return render(request, 'format/SPPD_KOP.html', context) 

# @require_http_methods(["GET"])
# def detail(request, slug):
#     try:
#         dtx = News.objects.get(slug=slug)
#         dtx.images = os.path.isfile(settings.MEDIA_ROOT+str(dtx.thumbnail))
#         dtx.seen += 1
#         dtx.save()
#     except News.DoesNotExist:
#         dtx = None

#     favorits = News.objects.filter(deleted_at=None).order_by('-seen')[:3]
#     for x in favorits:
#         x.thumbnail = {'url':x.thumbnail.url.replace('artikel/', 'artikel/thumb/')}

#     kategori = Category.objects.filter(typexs='news').order_by('nama')
#     otherpage = News.objects.filter(deleted_at=None).order_by('seen')[:2]
#     recent = News.objects.all().order_by('-created_at')[:3]
#     about = AboutMe.objects.get(jenis='header')

#     context = {
#         'title' : dtx.title if dtx != None else 'TIDAK DITEMUKAN',
#         'news' : dtx,
#         'cats' : kategori,
#         'favs' : favorits,
#         'othr' : otherpage,
#         'dt_tags': querytags('news'),
#         'recent' : recent,
#         'about' : about,
#         'type'  : 'detail',
#     }
#     return render(request, 'profile/news/detail_blog.html', context)


